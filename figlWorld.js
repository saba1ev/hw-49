var figlet = require('figlet');

figlet.text(process.argv[2], {
  font: 'Ghost',
  horizontalLayout: 'default',
  verticalLayout: 'default'
}, function(err, data) {
  if (err) {
    console.log('Something went wrong...');
    console.dir(err);
    return;
  }
  console.log(data);
});